package br.com.cwi.sicredi.mensageria.dto;

import java.io.Serializable;

public class ResultadoDTO implements Serializable {
	
	public String assembleia;
	public Integer idAssembleia;
	public Integer valorSim;
	public Integer valorNao;
	public boolean votacaoEncerrada;

}
